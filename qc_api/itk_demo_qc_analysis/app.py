from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from itk_demo_qc_analysis.endpoints import router


def create_app():
    app = FastAPI(
        title="ITk Pixel LLS QC API",
        description="Backend for ITkpix LLS QC analysis.",
        version="0.1.0",
        docs_url="/api/docs",  # custom location for swagger UI
    )
    app.include_router(router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )

    @app.get("/", include_in_schema=False)
    async def index():
        return RedirectResponse("/docs")

    app.mount("/output", StaticFiles(directory="/tmp/output"), name="output")

    return app
