import json
from abc import ABC
from enum import Enum
from typing import Optional
from pathlib import Path

from pydantic import BaseModel, Field
from rich import print
from rich.columns import Columns
from rich.pretty import pprint
from rich.table import Table

from pydantic import BaseModel


class FrontendID(BaseModel):
    uuid: str
    serial: str

class Module(BaseModel):
    uuid: str
    serial_number: str
    mod_pos_on_lls: int
    frontends: list[FrontendID]

class TestTypes(str, Enum):
    MininumHealthTest = "Minimum Health Test"
    PixelFailureTest = "Pixel Failure Analysis"
    DigitalOnlyTest = "Digital Only Test"

class AnalysisParams(BaseModel):
    module: Module
    analysis_type: TestTypes
    analysis_uuid: str
    analysis_tag: str
    scan_tag: str
    write_results_to_db: bool

class ScanInfo(BaseModel):
    name: str = ""
    db_prefix: str = ""
    path_on_disk: Path = None
    active: bool = False 

class ScanList():
    digitalscan: ScanInfo 
    analogscan: ScanInfo 
    thresholdscan_hr: ScanInfo 
    thresholdscan_hd: ScanInfo 
    noisescan: ScanInfo 
    totscan: ScanInfo 
    thresholdscan_zerobias: ScanInfo 
    discbumpscan: ScanInfo 
    mergedbumpscan: ScanInfo 
    sourcescan: ScanInfo 

    def __init__(self):
        self.digitalscan = ScanInfo(name="digitalscan", db_prefix="ds_")
        self.analogscan = ScanInfo(name="analogscan", db_prefix="as_")
        self.thresholdscan_hr = ScanInfo(name="thresholdscan_hr", db_prefix="hr_")
        self.thresholdscan_hd = ScanInfo(name="thresholdscan_hd", db_prefix="hd_")
        self.noisescan = ScanInfo(name="noisescan", db_prefix="ns_")
        self.totscan = ScanInfo(name="totscan", db_prefix="ts_")
        self.thresholdscan_zerobias = ScanInfo(name="thresholdscan_zerobias", db_prefix="tszb_")
        self.discbumpscan = ScanInfo(name="discbumpscan", db_prefix="db_")
        self.mergedbumpscan = ScanInfo(name="mergedbumpscan", db_prefix="mbs_")
        self.sourcescan = ScanInfo(name="sourcescan", db_prefix="sos_")

class AnalysisInfoBase(ABC):
    full_test_name: str
    workdir_prefix: str
    time_stamp_filename: str
    scan_list: ScanList 
    module_qc_test_name: str
    test_executable_name: str

    def get_scan_info_array(self) -> list[ScanInfo]:
        scan_info_array = []
        for scan in vars(self.scan_list).values():
            if isinstance(scan, ScanInfo) and scan.active:
                scan_info_array.append(scan)
        return scan_info_array

class MinimumHealthTestInfo(AnalysisInfoBase):
    full_test_name = TestTypes.MininumHealthTest
    workdir_prefix = "mht"
    time_stamp_filename = "Time_stamps_execution_minimum_health_test.txt"
    module_qc_test_name = "MIN_HEALTH_TEST"
    test_executable_name = "analysis-MIN-HEALTH-TEST"
    scan_list = ScanList()

    def __init__(self):
        self.scan_list = ScanList()
        self.scan_list.digitalscan.active = True
        self.scan_list.analogscan.active = True
        self.scan_list.thresholdscan_hr.active = True

class PixelFailureAnalysisInfo(AnalysisInfoBase):
    full_test_name = TestTypes.PixelFailureTest
    workdir_prefix = "pfa"
    time_stamp_filename = "Time_stamps_execution_pixel_failure_test.txt"
    module_qc_test_name = "PIXEL_FAILURE_ANALYSIS"
    test_executable_name = "analysis-PIXEL-FAILURE-ANALYSIS"

    def __init__(self):
        self.scan_list = ScanList()
        self.scan_list.digitalscan.active = True
        self.scan_list.analogscan.active = True
        self.scan_list.thresholdscan_hd.active = True
        self.scan_list.noisescan.active = True
        self.scan_list.discbumpscan.active = True
        self.scan_list.mergedbumpscan.active = True
        self.scan_list.sourcescan.active = True
        self.scan_list.thresholdscan_zerobias.active = True

class DigitalTestInfo(AnalysisInfoBase):
    full_test_name = TestTypes.DigitalOnlyTest
    workdir_prefix = "dsa"
    time_stamp_filename = "Time_stamps_execution_digital_analysis_only.txt"
    module_qc_test_name = "MIN_HEALTH_TEST"

    def __init__(self):
        self.scan_list = ScanList()
        self.scan_list.digitalscan.active = True

test_type_dict = {
    TestTypes.MininumHealthTest: MinimumHealthTestInfo,
    TestTypes.PixelFailureTest: PixelFailureAnalysisInfo,
    TestTypes.DigitalOnlyTest: DigitalTestInfo,
}


class MinimumHealthTestFEResults(BaseModel):
    property_: Optional[dict] = Field(None, alias="property")  # ClassVar[dict] = {"ANALYSIS_VERSION": "2.2.6"}
    Metadata: Optional[dict] = None  # = Field(default_factory=dict) #ClassVar[dict] = {"QC_LAYER": "L2"}
    MIN_HEALTH_THRESHOLD_MEAN: float
    MIN_HEALTH_THRESHOLD_SIGMA: float
    MIN_HEALTH_NOISE_MEAN: float
    MIN_HEALTH_NOISE_SIGMA: float
    MIN_HEALTH_TOT_MEAN: float
    MIN_HEALTH_TOT_SIGMA: float
    MIN_HEALTH_DEAD_DIGITAL: float
    MIN_HEALTH_BAD_DIGITAL: float
    MIN_HEALTH_DEAD_ANALOG: float
    MIN_HEALTH_BAD_ANALOG: float
    MIN_HEALTH_THRESHOLD_FAILED_FITS: float
    MIN_HEALTH_HIGH_ENC: float
    MIN_HEALTH_THRESHOLD_FAILED_FITS_INDEPENDENT: float
    MIN_HEALTH_HIGH_ENC_INDEPENDENT: float


class MinimumHealthTestFE(BaseModel):
    serialNumber: str
    testType: str
    passed: bool
    results: MinimumHealthTestFEResults


class MinimumHealthTestResults(BaseModel):
    minimum_health_test_results: list[MinimumHealthTestFE]



if __name__ == "__main__":
    with open("output/dump_mht_20UPGM22110471.json") as f:
        mht_json = json.load(
            f,
            # object_hook=lambda d: SimpleNamespace(**d),
            # object_hook=lambda d: MinimumHealthTestResults(**d),
        )
    # pprint(mht_json, expand_all=True)
    mht = MinimumHealthTestResults(**mht_json)
    print(mht)

    fet = []
    for fe in mht.minimum_health_test_results:
        results = {k: f"{v:7.2f}" for k, v in vars(fe.results).items() if k.startswith("MIN")}

        table = Table(
            "Result",
            "Value",
            title=fe.serialNumber,
        )
        for k, v in results.items():
            table.add_row(k, v)
        fet.append(table)

    print(Columns(fet))
