import reflex as rx
import random

class HistoState(rx.State):
    test_data: list[list[dict[str, int]]]
    histos: list[dict]
    
    histos = [{"x": [i for i in range (0,100)],"y":  [random.randint(8,15) for i in range (0,100)]}, {"x": [1, 2, 3],"y":  [10, 15 , 20]}]
    test_data = []
    for i in histos:
        values = [{"x": x, "y": y} for x, y in zip(i["x"], i["y"])]
        test_data.append(values)

    test_data2: list[dict]
    test_data2=[{"x": 1, "y": 10, "z": "test"}, {"x": 2, "y": 15, "z": 5}, {"x": 3, "y": 20, "z": 1}]

    def histos(data1:list):
        test_data: list[list[dict[str, int]]]
        histos: list[dict]
        
        histos = [{"x": [i for i in range (0,100)],"y":  [random.randint(8,15) for i in range (0,100)],"z":  [random.randint(8,15) for i in range (1,5)]}, {"x": [1, 2, 3],"y":  [10, 15 , 20],"z":  [10, 15 , 20]}]
        test_data = []
        for i in histos:
            values = [{"x": x, "y": y, "z": z} for x, y, z in zip(i["x"], i["y"], i["z"])]
            test_data.append(values)

        return rx.recharts.scatter_chart(
                    rx.recharts.scatter(
                        data=HistoState.test_data[0],
                        fill="#8884d8",
                    ),
                    rx.recharts.reference_line(
                        y=15,
                        stroke=rx.color("red", 8),
                    ),
                    rx.recharts.reference_line(
                        y=10,
                        stroke=rx.color("green", 8),
                    ),
                    rx.recharts.graphing_tooltip(),
                    rx.recharts.x_axis(data_key="x", type_="number", allow_decimals=False,
                                       label={
                                            "value": "X Axis Label",
                                            "position": "center",
                                            "dy": 12,
                                        }                  
                    ),
                    rx.recharts.y_axis(data_key="y", label={
                            "value": "Y Axis Label",
                            "angle": -90,
                            "position": "center",
                            "dx": -10
                        }
                    ),
                    width="95%",
                    height=300,
            ),