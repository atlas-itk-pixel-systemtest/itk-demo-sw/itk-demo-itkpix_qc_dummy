# Analysis Manager example

An minimal example setup for the analysis manager microservice. 

## Setup

### Prerequisite
Docker needs to be installed and running
User needs to be authenticated with the [CERN registry](https://demi.docs.cern.ch/faq/#error-401-authorization-required-when-pulling-from-cern-registry).
FAQ on frequent issues can be found [here](https://demi.docs.cern.ch/faq/), if you have further questions, feel free to ask via [mail](mailto:jonas.schmeing@cern.ch) or Mattermost.

## Setting up the environment
Execute
```
./config
```
to create a `.env` file with the necessary configuratuin values
- `HOST`: hostname of your system
- `PUID`: uid of your user
- `PGID`: gid of your user
- `PTZ`: timezone of your system
- `STACK`: used to group the containers
- `DOCKER_DIR`: a directory where docker can store persistent data, the docker user needs to have read and write access to the directory.
All necessary directories and files in the `DOCKER_DIR` will be automatically created.

The docker compose file includes the following services:
- `registry-compose.yaml`
    - registry: service registry that keeps track of running services and their metadata
    - etcd: backend for the registry
    - dashboard: overview gui for the registry
- `configdb-compose.yaml`
    - configdb-api: runkey database
    - runkey-ui: UI to view database
- `postgres-compose.yaml`
    - postgres: configdb backend
    - adminer: GUI for database backend
- `analysis-mangager-compose.yaml`
    - qc-api: backend container running the analysis scripts
    - analysis-manager-ui: gui to orchestrate analysis
If you don't need all of these services you can remove or comment them out of the docker compose files.

## Starting the containers
You can start all containers with
```
docker compose up -d
```

## Create lls runkey
Create an examplary runkey to view in the analysis manager:
- Change directory to example/lls
- Create venv
- Install requirements.txr 
- Execute create_lls.py script 

The create_lls.py script can be edited / expanded to import scan data from local yarr scans. A DAQ-UI for automating the scans is in development as well.


## Accessing the runkey manager
Once the containers are running the runkey ui will be accessible at http://localhost:3000/ or can be found over the dashboard at http://localhost/.
