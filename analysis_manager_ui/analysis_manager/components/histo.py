import reflex as rx
from analysis_manager.states.histo_state import HistoState

def histo() -> rx.Component:
    return rx.box(
        rx.vstack(
            rx.box(
                rx.heading("Histograms", align="left", size="8"),
                rx.heading(
                    "Test",
                    size="4",
                    margin_y="7px",
                ),
            ),
        ),
        rx.grid(
            #rx.foreach(HistoState.test_data, HistoState.histos),
            rx.recharts.scatter_chart(
                    rx.recharts.scatter(
                        data=HistoState.test_data[0],
                        fill="#8884d8",
                    ),
                    rx.recharts.reference_line(
                        y=15,
                        stroke=rx.color("red", 8),
                    ),
                    rx.recharts.reference_line(
                        y=10,
                        stroke=rx.color("green", 8),
                    ),
                    rx.recharts.x_axis(data_key="x", type_="number", allow_decimals=False),
                    rx.recharts.y_axis(data_key="y"),
                    width="95%",
                    height=300,
            ),
            rx.recharts.scatter_chart(
                    rx.recharts.scatter(
                        data=HistoState.test_data2,
                        fill="#8884d8",
                    ),
                    rx.recharts.reference_line(
                        y=15,
                        stroke=rx.color("red", 8),
                    ),
                    rx.recharts.reference_line(
                        y=10,
                        stroke=rx.color("green", 8),
                    ),
                    rx.recharts.graphing_tooltip(),
                    rx.recharts.x_axis(data_key="x", type_="number", allow_decimals=False,
                                       label={
                                            "value": "X Axis Label",
                                            "position": "center",
                                            "dy": 12,
                                        }                  
                    ),
                    rx.recharts.y_axis(data_key="y", label={
                            "value": "Y Axis Label",
                            "angle": -90,
                            "position": "center",
                            "dx": -10
                        }
                    ),
                    rx.recharts.z_axis(data_key="z"),
                    width="95%",
                    height=300,
            ),
            rx.recharts.scatter_chart(
                    rx.recharts.scatter(
                        data=HistoState.test_data[0],
                        fill="#8884d8",
                    ),
                    rx.recharts.reference_line(
                        y=15,
                        stroke=rx.color("red", 8),
                    ),
                    rx.recharts.reference_line(
                        y=10,
                        stroke=rx.color("green", 8),
                    ),
                    rx.recharts.x_axis(data_key="x", type_="number", allow_decimals=False),
                    rx.recharts.y_axis(data_key="y"),
                    width="95%",
                    height=300,
            ),
            rx.recharts.scatter_chart(
                    rx.recharts.scatter(
                        data=HistoState.test_data[0],
                        fill="#8884d8",
                    ),
                    rx.recharts.reference_line(
                        y=15,
                        stroke=rx.color("red", 8),
                    ),
                    rx.recharts.reference_line(
                        y=10,
                        stroke=rx.color("green", 8),
                    ),
                    rx.recharts.x_axis(data_key="x", type_="number", allow_decimals=False),
                    rx.recharts.y_axis(data_key="y"),
                    width="95%",
                    height=300,
            ),
            #rx.foreach(HistoState.test_data, HistoState.histos),
            columns="2",
            width="100%",
            spacing_y="7"
        ),
        rx.recharts.scatter_chart(
                    rx.recharts.scatter(
                        data=HistoState.test_data[0],
                        fill="#8884d8",
                    ),
                    rx.recharts.reference_line(
                        y=15,
                        stroke=rx.color("red", 8),
                    ),
                    rx.recharts.reference_line(
                        y=10,
                        stroke=rx.color("green", 8),
                    ),
                    rx.recharts.graphing_tooltip(),
                    rx.recharts.x_axis(data_key="x", type_="number", allow_decimals=False,
                                       label={
                                            "value": "X Axis Label",
                                            "position": "center",
                                            "dy": 20,
                                        }                  
                    ),
                    rx.recharts.y_axis(data_key="y", label={
                            "value": "Y Axis Label",
                            "angle": -90,
                            "position": "center",
                            "dx": -10
                        }
                    ),
                    width="95%",
                    height=300,
            ),
        background_color=rx.color("gray", 3),
        width="100%",
        height="100%"
    )