from pyregistry import ServiceRegistry
from dotenv import load_dotenv
import os
import uvicorn

stack = "wupp-charon"
host = "wupp-charon.cern.ch"

load_dotenv()
os.environ["CONFIGDB_KEY"] = f"demi/{stack}/itk-demo-configdb/api/url"
os.environ["USE_LOCAL_SCAN_DATA"] = "0"
os.makedirs("/tmp/output", exist_ok=True)

from itk_demo_qc_analysis.app import create_app  # noqa: E402, needed here to get env vars

sr_url = f"http://{host}:5111/api"
sr = ServiceRegistry(sr_url)
sr.set(f"demi/{stack}/itk-demo-qc-analysis/api/url", f"http://{host}:5112/api")

if __name__ == "__main__":
    app = create_app()
    uvicorn.run(app, host="0.0.0.0", port=5112)