import logging
import json
import reflex as rx
from collections import defaultdict
from pyconfigdb import ConfigDB
from analysis_manager.config import config
from analysis_manager.states.lls_state import LLSState
from analysis_manager.states.messages_state import MessagesState
from analysis_manager.analysis_models import analysis_types, AnalysisFeatures
from analysis_manager.models import SummaryHistogram
from analysis_manager.PDB_models import (
    PDBMinimumHealthTestFrontEndResults,
    PDBScanTestProperties,
)
from analysis_manager.tools import upper_limit

log_sars = logging.getLogger("Analysis_Manager_Logger")


class ScanAnalysisResultsState(rx.State):
    selected_analysis_tag: str = "none"
    loaded_analysis_tag: str = "none"
    show_results_legend: bool = False
    analysis_tag_list_placeholder: str = "Select an analysis tag"
    analysis_type: str = "none"
    analysis_label: str = "Analysis"
    results_type: str = "Summary"
    analysis_tag_check: bool = False
    summary_histograms: list[SummaryHistogram] = []

    async def check_analysis_tag_input(self, value: str):
        llsstate: LLSState = await self.get_state(LLSState)
        if value in llsstate.list_analysis_tags:
            self.selected_analysis_tag = value
            self.analysis_tag_check = True
        else:
            self.analysis_tag_check = False

    @rx.event
    async def reset_analysis_tag_info(self):
        self.summary_histograms.clear()
        llsstate: LLSState = await self.get_state(LLSState)
        for m in llsstate.mod_info:
            m.label_color = "silver"
            m.QC_result = False
            m.serialnumber_color = "lightslategray"
            for fe in m.frontends:
                # fe.serialnumber = "none"
                fe.testresult = False
                fe.results.clear()
                # for te in fe.results:
                #     te.color = "gainsboro"

    @rx.event
    async def load_tagged_ana_results(self):
        llsstate: LLSState = await self.get_state(LLSState)
        ms: MessagesState = await self.get_state(MessagesState)
        ms.add_message(
            f"load_tagged_ana_results(): load results tagged with {self.selected_analysis_tag}"
        )
        db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
        lls_result_data = db.tag_tree(name=llsstate.selected_LLS_ID, stage=False)
        analysis_result_data = db.search_in_tag(
            self.selected_analysis_tag, order_by_object=True, payload_data=True
        )
        log_sars.debug(f"load_tagged_ana_results():\n{lls_result_data}")
        await self.reset_analysis_tag_info()

        histo_dict = defaultdict(list)

        for m in llsstate.mod_info:
            m.label_color = "black"
            module_QC_result = True
            n_errors_occured = 0
            fe_found = False
            check_type = "none"

            for i, fe in enumerate(m.frontends):
                log_sars.debug(
                    f"Frontend {fe.serialnumber} from module {m.position} with serial number: {m.module_serial}."
                )

                # Reset the analysis results for all front-ends:
                # Need to add check for analysis typer later:
                # fe.mht_report = mht_pdb_report

                # Get list of analyses for the module (empty list if no analyses are found):
                list_of_analyses_for_frontend = analysis_result_data.get(fe.uuid, [])
                n_analyses = len(list_of_analyses_for_frontend)
                if n_analyses > 0:
                    log_sars.debug(
                        f"number of analyses for frontend {fe.serialnumber} from the module at position {m.position}: {n_analyses}"
                    )
                    if n_analyses != 1:
                        m.label_color = "darkslateblue"
                    # As intermediate solution we choose the first analysis if there are more than one:
                    frontend_result = json.loads(
                        list_of_analyses_for_frontend["analysis_results_payload"][
                            "data"
                        ]
                    )
                    fe.results.clear()

                    if frontend_result:
                        # log_sars.info(f"{fe_res}")
                        fe_found = True
                        module_QC_result = module_QC_result & frontend_result["passed"]
                        # print(f"passed = {fe_res["passed"]}")
                        fe.testtype = frontend_result["testType"]
                        if check_type == "none":
                            check_type = fe.testtype
                        else:
                            if fe.testtype == check_type:
                                pass
                            else:
                                n_errors_occured += 1
                                ms.add_message("ERROR: test types do not agree")
                        fe.testresult = frontend_result["passed"]

                        # Do the three tests:
                        # print(f"module serial number = {m.module_serial}, fe serial = {fe.serialnumber}")
                        analysis: AnalysisFeatures = analysis_types[fe.testtype]()
                        for test in analysis.results:
                            for test_var in test.variables:
                                test_var.value = int(
                                    frontend_result["results"][test_var.name]
                                )
                                position = f"M{m.position}.{fe.position}"
                                histo_dict[test_var.name].append(
                                    (test_var.value, position)
                                )

                        fe_results = analysis.run_test()
                        fe.results.extend(fe_results)

                        # Fill the analysis results in the PDB FE-results objects
                        if fe.testtype == fe.mht_fe_report.testType:
                            fe.mht_fe_report.passed = bool(frontend_result["passed"])
                            # Create a dictionary of the Pydantic object to extract the results information
                            # from the JSON object obtained from the database.
                            pdb_fe_prop_dict = fe.mht_fe_report.properties.model_dump()
                            for var in pdb_fe_prop_dict:
                                if var in frontend_result["results"]["property"]:
                                    pdb_fe_prop_dict[var] = frontend_result["results"][
                                        "property"
                                    ][var]
                            fe.mht_fe_report.properties = PDBScanTestProperties(
                                **pdb_fe_prop_dict
                            )
                            fe.mht_fe_report.properties.MEASUREMENT_VERSION = (
                                self.selected_analysis_tag
                            )
                            pdb_fe_res: PDBMinimumHealthTestFrontEndResults = (
                                fe.mht_fe_results
                            )
                            mht_fe_results_dict = pdb_fe_res.model_dump()
                            for variable in mht_fe_results_dict:
                                read_value = frontend_result["results"][variable]
                                if isinstance(read_value, float):
                                    mht_fe_results_dict[variable] = str(read_value)
                                else:
                                    mht_fe_results_dict[variable] = read_value
                            fe.mht_fe_results = PDBMinimumHealthTestFrontEndResults(
                                **mht_fe_results_dict
                            )

                        if fe.testtype == fe.pfa_report["testType"]:
                            fe.pfa_report["properties"]["ANALYSIS_VERSION"] = (
                                frontend_result[
                                    "results"
                                ]["property"]["ANALYSIS_VERSION"]
                            )
                            fe.pfa_report["properties"]["YARR_VERSION"] = (
                                frontend_result["results"]["property"]["YARR_VERSION"]
                            )
                            fe.pfa_report["properties"]["MEASUREMENT_DATE"] = (
                                frontend_result[
                                    "results"
                                ]["property"]["MEASUREMENT_DATE"]
                            )
                            fe.pfa_report["passed"] = frontend_result["passed"]
                            for variable in fe.pfa_report["results"]:
                                fe.pfa_report["results"][variable] = frontend_result[
                                    "results"
                                ][variable]
                            print(fe.pfa_report)

                if fe_found:
                    # Print the module level QC result:
                    # ms.add_message(f"load_tagged_ana_results(): {module_QC_result}")
                    m.QC_result = module_QC_result
                    if module_QC_result == True:
                        m.serialnumber_color = "lime"
                    else:
                        m.serialnumber_color = "red"
                    # Set the analysis type of the panel
                    if n_errors_occured == 0:
                        self.analysis_type = check_type
                        if check_type == analysis.testtype:
                            self.analysis_label = analysis.analysis_label

        # Create histograms
        for test in analysis.results:
            for test_var in test.variables:
                data = []
                for i, (value, position) in enumerate(histo_dict[test_var.name]):
                    data.append({"x": i, "y": value, "FE": position})
                self.summary_histograms.append(
                    SummaryHistogram(
                        name=test_var.name,
                        data=data,
                        upper=upper_limit[test_var.name],
                        lower=0,
                        y_label=test_var.description,
                    )
                )

        # Finally, set the loaded analysis tag:
        self.loaded_analysis_tag = self.selected_analysis_tag

    def toggle_results_legend(self):
        self.show_results_legend = not self.show_results_legend

    def toggle_results_type(self):
        if self.results_type == "Summary":
            self.results_type = "Histograms"
        else:
            if self.results_type == "Histograms":
                self.results_type = "Summary"
