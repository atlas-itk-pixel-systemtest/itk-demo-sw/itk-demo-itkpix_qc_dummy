import reflex as rx
import logging
from pyconfigdb import ConfigDB
from analysis_manager.config import config
import json
from requests import post
from pyregistry import ServiceRegistry

log = logging.getLogger("Analysis_Manager_Logger")

payload = {
    "OB_LOADED_LONGERON": {
        "attachments": []
    },
    "PP0_LONGERON": {
        "attachments": []
    },
    "OB_LOADED_INCLINED_HALF-RING": {
        "attachments": []
    },
    "OB_INCLINED_PP0": {
        "attachments": []
    },
    "OB_VIRTUAL_HV_GROUP": {
        "attachments": []
    },
    "OB_LOADED_MODULE_CELL": {
        "attachments": []
    },
    "MODULE": {
        "attachments": []
    },
    "BARE_MODULE": {
        "attachments": []
    },
    "FE_CHIP": {
        "attachments": [
        {
            "type": "config",
            "title": "L2_warm"
        }
        ]
    },
    "SENSOR_TILE": {
        "attachments": []
    }
}

class PDBDownloadState(rx.State):
    got_lls_info = False
    list_LLS_serials: list[str] = []
    selected_LLS_serial: str = "none"
    counter = 0
    use_db = True
    component_serial: str
    runkey: str
    result: str
    runkey_ui_url:str
    get_pdb_component_loading = False
    get_pdb_component_status = False
    lls_data: list
    nLLS: int
    got_LLS_info: bool
    select_placeholder: str

    def getLLSInfoTest(self):
        self.list_LLS_serials.append(f"LLS0{self.counter}")
        self.counter += 1

    def getLLSInfo(self):
        self.got_lls_info = True
        log.info("getLLS_Info(): Use the LLS QC DB to obtain the LLS information.")
        db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
        self.lls_data = db.read_all(table="tag", stage=False, name_filter="LLS", order_by="name", filter="lls")
        log.debug(self.lls_data)
        self.nLLS = len(self.lls_data)
        self.list_LLS_serials = [d["name"] for d in self.lls_data]
        log.debug(self.list_LLS_serials)
        # Test if there are duplicates in the list of LLS:
        if len(self.list_LLS_serials) == len(set(self.list_LLS_serials)):
            log.debug("Test for duplicates passed!")
        else:
            log.warning("WARNING: There as duplicates in the list of LLS.")
            self.found_duplicate_LLS = True
        if self.nLLS > 0:
            self.got_LLS_info = True
            self.select_placeholder = "Select an LLS ID"
    
    async def get_pdb_component(self):
        self.get_pdb_component_loading=True
        self.get_pdb_component_status=False
        self.result=""
        yield

        if self.runkey == "":
            self.result="Please enter runkey"
        
        else:
            registry = ServiceRegistry(srUrl=config.sr_url)
            pdp_api_url = registry.get(key=config.pdb_api_key)
            response = post(
                url=f"{pdp_api_url}/component/tree",
                params={"componentSerial": self.component_serial, "configdb": self.runkey, "metadata": True},
                data=json.dumps(payload),
                headers={"content-type": "application/json", "accept": "application/json"},
            )
            
            if response.status_code == 200:
                self.get_pdb_component_status=True
                registry = ServiceRegistry(srUrl=config.sr_url)
                self.runkey_ui_url = registry.get(key=config.runkey_ui_key)


            else:
                self.result = "not successful"
        self.get_pdb_component_loading=False
    
    