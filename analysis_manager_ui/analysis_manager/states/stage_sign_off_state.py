import reflex as rx
import json
from analysis_manager.states.lls_state import LLSState


class StageSignOffState(rx.State):
    mht_analysis_tag: str = "none"
    pfa_analysis_tag: str = "none"
    show_esummary: bool = False
    modpos_of_esummary: int = 0
    esummary_text: str
    fe_mht_reports_text: str

    @rx.event
    async def copy_esummary(self, modpos: int):
        llsstate: LLSState = await self.get_state(LLSState)
        self.show_esummary = True
        self.modpos_of_esummary = modpos
        modinfo_selected = next(mi for mi in llsstate.mod_info if (mi.position == modpos))
        dumped_e_summary = json.dumps(modinfo_selected.e_summary)
        dumped_e_summary = dumped_e_summary.replace(": {", ":\n{")
        self.esummary_text = dumped_e_summary.replace(",", ",\n")
        # Reset the FE-report string:
        self.fe_mht_reports_text = "{\n"
        for fe in modinfo_selected.frontends:
            fe_rep_str = json.dumps(fe.mht_fe_report.model_dump())
            fe_rep_str += ', "results": '
            fe_rep_str += json.dumps(fe.mht_fe_results.model_dump())
            fe_rep_str = fe_rep_str.replace(",", ",\n")
            fe_rep_str = fe_rep_str.replace(": {", ":\n{")
            fe_rep_str += "\n},\n"
            self.fe_mht_reports_text += fe_rep_str
        self.fe_mht_reports_text += "}"
