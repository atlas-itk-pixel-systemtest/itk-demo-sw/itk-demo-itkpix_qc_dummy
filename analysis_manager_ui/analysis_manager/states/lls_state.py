import reflex as rx
import logging
import json
from datetime import datetime
from pyregistry import ServiceRegistry
from analysis_manager.config import config
from analysis_manager.states.messages_state import MessagesState
from pyconfigdb import ConfigDB
from pyconfigdb.exceptions import ConfigDBResponseError as CRE
from analysis_manager.models import ModuleInfo, Frontend
from analysis_manager.PDB_models import (
    PDBMinimumHealthTestFrontEndResults,
    PDBScanTestProperties,
    PDBMinimumHealthTestFrontEndReport,
    pfa_pdb_report,
    module_e_summary
)

log_lls = logging.getLogger("Analysis_Manager_Logger")
class LLSState(rx.State):
    got_LLS_info: bool = False
    nLLS: int = 0
    lls_data = []
    list_LLS_ids: list[str]
    found_duplicate_LLS: bool = False
    selected_LLS_ID: str = "none"
    type_selected: str = "none"
    select_placeholder: str = "LLS not available"
    loaded_LLS_ID: str = "none"
    nMods_of_LLS: int = 0
    lls_id_check: bool = True
    lls_ids_datalist: str
    mod_info: list[ModuleInfo]
    list_scan_tags: list[str]
    list_analysis_tags: list[str]
    n_analysis_tags: int =  0
    analysis_tags_datalist: str
    scan_tags_datalist: str
    config_db_url: str = ServiceRegistry(srUrl=config.sr_url).get(key=config.configdb_key)

    async def check_lls_id_input(self, value: str):
        if value in self.list_LLS_ids:
            self.selected_LLS_ID = value
            self.lls_id_check = True
        else:
            self.lls_id_check = False

    async def getLLS_Info(self):
        ms = await self.get_state(MessagesState)
        if config.use_db:
            log_lls.info(f"getLLS_Info(): Use the LLS QC DB at {config.sr_url} to obtain the LLS information.")
            db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
            self.lls_data = db.read_all(table="tag", 
                                        stage=False, 
                                        name_filter="LLS", 
                                        order_by="name", 
                                        filter="lls")
        else:
            # Read a JSON file with the LLS information
            ms.add_message("getLLS_Info(): Reading JSON file with the LLS information.")
            with open("./example_lls_keys/LLS_OB_CERN_collection.json") as f:
                lls_data_loc = json.load(f)
                # print(lls_data_loc)
                self.lls_data = lls_data_loc["list"]
        log_lls.debug(self.lls_data)
        self.nLLS = len(self.lls_data)
        self.list_LLS_ids = [d["name"] for d in self.lls_data]
        log_lls.debug(self.list_LLS_ids)
        ms.add_message(f"There are {self.nLLS} LLS.")
        # Test if there are duplicates in the list of LLS:
        if len(self.list_LLS_ids) == len(set(self.list_LLS_ids)):
            log_lls.debug("Test for duplicates passed!")
        else:
            log_lls.warning("WARNING: There as duplicates in the list of LLS.")
            self.found_duplicate_LLS = True
        if self.nLLS > 0:
            self.got_LLS_info = True
            self.select_placeholder = "Select an LLS ID"

        sorted_list_LLS_ids = sorted(self.list_LLS_ids)
        self.lls_ids_datalist = "<datalist id='lls_ids_datalist'>"
        for id in sorted_list_LLS_ids:
            self.lls_ids_datalist = (
                self.lls_ids_datalist + f"<option value={id}></option>"
            )
        self.lls_ids_datalist = self.lls_ids_datalist + "</datalist>"

    async def load_prop_selected(self):
        ms = await self.get_state(MessagesState)
        if (self.selected_LLS_ID == "none"):
            ms.add_message("load_prop_selected(): ERROR: No LLS was selected.")
            return None
        selected_lls_runkey = next(d for d in self.lls_data if (d["name"] == self.selected_LLS_ID))
        log_lls.info(f"Selected LLS runkey:\n{selected_lls_runkey}")
        self.type_selected = selected_lls_runkey["metadata"]["lls_type"]
        self.nMods_of_LLS = int(selected_lls_runkey["metadata"]["nMods"])
        log_lls.info(f"Number of modules mounted on the selected LLS = {self.nMods_of_LLS}")
        ms.add_message(f"load_prop_selected(): the LLS with the ID = {self.selected_LLS_ID} ({self.type_selected}) was selected and has {self.nMods_of_LLS} modules.")
        #self.selected_lls_payload_id = next(d["id"] for d in selected_lls_runkey["metadata"] if (d["type"] == "runkey_meta"))
        #log.debug(f"Selected runkey metadata payload id {self.selected_lls_payload_id}")
        # Obtain information on the modules mounted on the LLS:
        if config.use_db == True:
            log_lls.debug("load_prop_selected(): Access the LLS QC DB to obtain information on the modules mounted on the LLS.")
            db = ConfigDB(config.configdb_key, srUrl=config.sr_url)
            read_lls_data = db.tag_tree(name=self.selected_LLS_ID, stage=False)
        else:
            log_lls.info("load_prop_selected(): Read a JSON file to obtain information on the modules mounted on the LLS.")
            with open(f"./example_lls_keys/{self.selected_LLS_ID}.json") as f:
                read_lls_data = json.load(f)
        log_lls.debug(read_lls_data)
        # Check content:
        name_lls_selected = read_lls_data["name"]
        log_lls.info(f"load_prop_selected(): ID of the selected LLS read from JSON object = {name_lls_selected}")
        if name_lls_selected != self.selected_LLS_ID:
            log_lls.error(
                f"ID read from file / db does not agree with the ID of the selected LLS!!! {name_lls_selected} != {self.selected_LLS_ID}"
            )
        # Get new data:
        # Fill the lists with the properties of the newly selected LLS:
        modules = next(d["children"] for d in read_lls_data["objects"] if d["type"] == "modules")
        self.loaded_LLS_ID = self.selected_LLS_ID
        # Test new method:
        self.mod_info.clear()
        time_stamp = datetime.now().strftime("%Y-%m-%d_%H%M")
        for mod in modules:
            frontends: list[Frontend] = []

            for child in mod["children"]:
                if child["type"] == "frontend":
                    pfa_dict = pfa_pdb_report
                    pfa_dict["institution"] = config.institution
                    pfa_dict["date"] = time_stamp 
                    fe = Frontend(serialnumber=child["metadata"]["serial"], 
                            uuid=child["id"], 
                            position=child["metadata"]["position"],
                            mht_fe_report=PDBMinimumHealthTestFrontEndReport(
                                component = child["metadata"]["serial"],
                                institution = config.institution,
                                date = time_stamp,
                                properties=PDBScanTestProperties()
                                ),
                            mht_fe_results=PDBMinimumHealthTestFrontEndResults(),
                            pfa_report=pfa_dict
                            )
                    frontends.append(fe)
            frontends = sorted(frontends, key=lambda frontend: frontend.position)

            e_summary_dict = module_e_summary
            e_summary_dict["component"] = mod["metadata"]["serial"]
            e_summary_dict["institution"] = config.institution
            e_summary_dict["date"] = time_stamp
            self.mod_info.append(
                ModuleInfo(position=mod["metadata"]["position"], 
                            module_uuid=mod["id"],
                            module_serial=mod["metadata"]["serial"], 
                            checked_for_analysis=False, 
                            label_color="silver", 
                            QC_result=False, 
                            serialnumber_color="lightslategray",
                            e_summary=e_summary_dict, 
                            frontends=frontends)
                    )
            
        self.mod_info = sorted(self.mod_info, key=lambda x: x.position)
        log_lls.debug(self.mod_info)

        # Load information on scan tags
        self.list_scan_tags = await self.load_tags(f"{self.selected_LLS_ID}_scan-tags")

        sorted_list_scan_tags = sorted(self.list_scan_tags)
        self.scan_tags_datalist = "<datalist id='scan_tags_datalist'>"
        for tag in sorted_list_scan_tags:
            self.scan_tags_datalist = (
                self.scan_tags_datalist + f"<option value={tag}></option>"
            )
        self.scan_tags_datalist = self.scan_tags_datalist + "</datalist>"

        await self.load_analysis_tags()

    async def load_tags(self, tag_name: str):
        log_lls.info("Loading scan tags: ")
        db: ConfigDB = ConfigDB(config.configdb_key, srUrl=config.sr_url)
        if (db):
            try:
                tag_dict = db.tag_read(name=tag_name, stage=False)
            except CRE as e:
                if e.status == 463:
                    log_lls.warning(f"No tag with the name {tag_name} found.")
                    return []
                else:
                    raise e
            return tag_dict["members"]

    async def load_analysis_tags(self):
        analysis_tags = await self.load_tags(f"{self.selected_LLS_ID}_analysis_tags")
        log_lls.info(f"Analysis tags: {analysis_tags}")
        self.list_analysis_tags = analysis_tags
        if not analysis_tags:
            # Clear the list of analysis tags
            self.list_analysis_tags.clear()
        self.n_analysis_tags = len(self.list_analysis_tags)

        sorted_list_analysis_tags = sorted(self.list_analysis_tags)
        self.analysis_tags_datalist = "<datalist id='analysis_tags_datalist'>"
        for tag in sorted_list_analysis_tags:
            self.analysis_tags_datalist = (
                self.analysis_tags_datalist + f"<option value={tag}></option>"
            )
        self.analysis_tags_datalist = self.analysis_tags_datalist + "</datalist>"
