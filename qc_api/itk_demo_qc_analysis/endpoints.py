import logging

from fastapi import APIRouter, BackgroundTasks, HTTPException, Query, Request
from fastapi.responses import Response
from itk_demo_qc_analysis.models import AnalysisParams
from itk_demo_qc_analysis.analyses import (
    dummy_analog_analysis,
    dummy_digital_analysis,
    dummy_threshold_analysis,
    scan_analysis,
)

router = APIRouter(prefix="/api")


logging.basicConfig(level=logging.INFO)


@router.get("/health/", summary="health endpoint of itkpix_qc_dummy server")
async def health():
    return {"state": "active", "status": 200}


@router.get(
    "/analog_scan/",
    summary="Analyse the analog scan data of the specified front-end.",
)
async def ana_analog_scan(
    module_serial: str = Query(
        default=None,
        example="20UPGM22110466",
        description="ID of the module to analyse",
    ),
    frontend_ID: str = Query(default=None, example="12345", description="ID of the frontend to analyse"),
):
    # Call QC tools
    dummy_analog_analysis(module_serial, frontend_ID)
    response = f"analysis of {module_serial} / {frontend_ID}"
    return response


@router.get(
    "/digital_scan/",
    summary="Analyse the digital scan data of the specified front-end.",
)
async def ana_digital_scan(
    module_serial: str = Query(
        default=None,
        example="20UPGM22110466",
        description="ID of the module to analyse",
    ),
    frontend_ID: str = Query(default=None, example="12345", description="ID of the frontend to analyse"),
):
    # Call QC tools
    dummy_digital_analysis(module_serial, frontend_ID)
    response = f"analysis of {module_serial} / {frontend_ID}"
    return response


@router.get(
    "/threshold_scan/",
    summary="Analyse the threshold scan data of the specified front-end.",
)
async def ana_threshold_scan(
    module_serial: str = Query(
        default=None,
        example="20UPGM22110466",
        description="ID of the module to analyse",
    ),
    frontend_ID: str = Query(default=None, example="12345", description="ID of the frontend to analyse"),
):
    # Call QC tools
    dummy_threshold_analysis(module_serial, frontend_ID)
    response = f"analysis of {module_serial} / {frontend_ID}"
    return response


@router.get(
    "/all_scans/",
    summary="Analyse the all scan data (analog, digital and threshold) of the specified front-end.",
)
async def ana_all_scans(
    module_serial: str = Query(
        default=None,
        example="20UPGM22110466",
        description="ID of the module to analyse",
    ),
    frontend_ID: str = Query(default=None, example="12345", description="ID of the frontend to analyse"),
):
    # Call QC tools
    dummy_analog_analysis(module_serial, frontend_ID)
    dummy_digital_analysis(module_serial, frontend_ID)
    dummy_threshold_analysis(module_serial, frontend_ID)
    response = f"analysis of {module_serial} / {frontend_ID}"
    return response


@router.post(
    "/minimum_health/",
    summary="Perform the minimum health test of the specified module.",
)
async def ana_minimum_health(ap: AnalysisParams, background_tasks: BackgroundTasks):
    # Call QC tools
    # minimum_health_test(mht)
    # schedule the minimum health test to run in the background:
    background_tasks.add_task(scan_analysis, ap)
    response = Response(
        content=f"{ap.analysis_type} of the module at position {ap.module.mod_pos_on_lls} started as background task.", 
        media_type="text/plain", 
        status_code=202
    )
    # print(ap)
    return response
