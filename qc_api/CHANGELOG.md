# Changelog

All notable changes[^1] to the `itk-demo-qc-analysis`

This repo is "living at head" which means tags correspond to the overall repo.
All subrepos/workspaces, all packages in the package registry and all container images in the image registry follow the repo tag.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
 - Changed type of health endpoint to int

## [1.0.0] - 2024-04-12

