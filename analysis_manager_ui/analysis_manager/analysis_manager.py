# Analysis Manager for loaded-local support QC

import reflex as rx
from analysis_manager.states.analysis_manager_state import AnalysisManagerState
from analysis_manager.states.messages_state import MessagesState as ms
from analysis_manager.states.lls_state import LLSState
from analysis_manager.components.lls_panel import LLS_panel
from analysis_manager.components.composer_panel import composer_panel
from analysis_manager.components.results_panel import results_panel
from analysis_manager.components.utilities import error_box_loading
from analysis_manager.components.stage_sign_off_panel import stage_sign_off_panel
from analysis_manager.components.pdb_download_panel import pdb_download
from analysis_manager.components.histo import histo


def navigation_panel() -> rx.Component:
    return rx.box(
        rx.button(
            "Analysis Composition",
            on_click=AnalysisManagerState.change_main_panel("analysis_composition"),
            border_bottom_left_radius="0px",
            border_bottom_right_radius="0px",
            border_top_right_radius="0px",
            border_top_left_radius="10px",
            background_color=AnalysisManagerState.tab_colors[0],
            color="dimgray",
            border_width="1px",
            border_bottom_width="0px",
            border_color="black",
        ),
        rx.button(
            "Results Viewer",
            on_click=AnalysisManagerState.change_main_panel("results_viewer"),
            border_radius="0px",
            background_color=AnalysisManagerState.tab_colors[1],
            color="dimgray",
        ),
        rx.button(
            "Electrical Tests",
            on_click=AnalysisManagerState.change_main_panel("electrical_tests"),
            border_radius="0px",
            background_color=AnalysisManagerState.tab_colors[2],
            color="dimgray",
            # border_left_width="1px",
            # border_width="1px",
            # border_color="red"
        ),
        rx.button(
            "Stage Sign Off",
            on_click=AnalysisManagerState.change_main_panel("stage_sign_off"),
            border_radius="0px",
            background_color=AnalysisManagerState.tab_colors[3],
            color="dimgray",
        ),
        width="100%",
        background_color="lightgray",
        border_top_left_radius="10px",
        border_top_right_radius="10px",
        border_color="gainsboro",
        border_width="1px",
    )


def main_panel() -> rx.Component:
    return rx.flex(
        navigation_panel(),
        rx.box(
            rx.match(
                AnalysisManagerState.main_panel_type,
                ("analysis_composition", composer_panel()),
                ("results_viewer", results_panel()),
                ("electrical_tests", electrical_tests_panel()),
                ("stage_sign_off", stage_sign_off_panel()),
            ),
            min_height="35vh",
            # border_top_left_radius="0px",
            border_bottom_right_radius="10px",
            border_bottom_left_radius="10px",
            background_color="white",
            border_width="1px",
            border_color="gainsboro",
            padding="10px",
        ),
        width="100%",
        direction="column",
    )


def electrical_tests_panel() -> rx.Component:
    return rx.flex(
        rx.heading("Electrical Tests", size="6"),
        rx.cond(
            (
                (LLSState.loaded_LLS_ID == LLSState.selected_LLS_ID)
                & (LLSState.selected_LLS_ID != "none")
                & (LLSState.nMods_of_LLS > 0)
            ),
            rx.flex(
                rx.heading("Not implemented yet.\n\n\n\n", color="purple"),
                background_color=rx.color("blue", 4),
                border_radius="10px",
                padding="10px",
                width="100%",
            ),
            error_box_loading(),
        ),
        spacing="2",
        direction="column",
    )


def message_panel() -> rx.Component:
    # Panel to show stdout messages of the UI backend
    return rx.card(
        rx.flex(
            rx.flex(
                rx.heading("Messages", size="6"),
                rx.flex(
                    rx.text("Show last"),
                    rx.select(
                        ["10", "20", "50"],
                        default_value="20",
                        on_change=ms.set_max_messages,
                    ),
                    spacing="2",
                ),
                justify="between",
            ),
            rx.scroll_area(
                rx.flex(
                    rx.foreach(ms.messages, message_text),
                    direction="column",
                    spacing="1",
                    padding_right="12px",
                ),
                type="always",
                scrollbars="vertical",
                style={"height": 300},
            ),
            direction="column",
            spacing="2",
        ),
        width="100%",
    )


def message_text(message: str) -> rx.Component:
    return rx.code(message, size="1")


def index() -> rx.Component:
    # Welcome Page (Index)
    return rx.box(
        rx.vstack(
            rx.box(
                rx.heading("QC Analysis Manager", align="left", size="9"),
                rx.heading(
                    "for Outer Barrel loaded local-support (LLS) structures",
                    size="6",
                    margin_y="7px",
                ),
                margin="10px",
            ),
            rx.hstack(
                rx.vstack(LLS_panel(), message_panel(), width="30%"),
                rx.vstack(main_panel(), width="80%"),
                width="100%",
            ),
            margin_left="10px",
            width="99%",
        ),
        rx.box(rx.logo(), width="15%"),
        background_color=rx.color("gray", 3),
        width="100%",
        height="100%",
    )


app = rx.App(
    theme=rx.theme(
        appearance="light",
        has_background=True,
        radius="large",
        #accent_color="green",
    )
)
app.add_page(index)
app.add_page(pdb_download, route="/pdb")
app.add_page(histo, route="/histo")

