import reflex as rx
from analysis_manager.models import ModuleInfo
from analysis_manager.states.lls_state import LLSState
from analysis_manager.states.stage_sign_off_state import StageSignOffState
from analysis_manager.components.utilities import error_box_loading
from analysis_manager.components.utilities import content_box_style

def stage_sign_off_panel() -> rx.Component:
    return rx.flex(
        rx.heading("Stage Sign Off", size="6"),
        rx.cond(
            ((LLSState.loaded_LLS_ID == LLSState.selected_LLS_ID)
                & (LLSState.selected_LLS_ID != "none")
                & (LLSState.nMods_of_LLS > 0)
            ),
            rx.flex(
                tag_selection_pads(),
                LLS_results_summary_pad(),
                rx.cond(
                    StageSignOffState.show_esummary,
                    report_flex(),
                    rx.text("")
                ),
                direction="column",
                spacing="2"
            ),
            error_box_loading()
        ),
        spacing="2",
        direction="column"
    )

def select_mht_tag_flex() -> rx.Component:
    return rx.flex(
        rx.select(
            LLSState.list_analysis_tags,
            on_change=StageSignOffState.set_mht_analysis_tag,
            placeholder="Select an analysis tag",
            label="Select an analysis tag",
            width="60%"
        )
    )

def tag_selection_pads() -> rx.Component:
    return rx.flex(
        rx.flex(
            rx.text("Minimum Health Test",
                    weight="bold"),
            select_mht_tag_flex(),
            rx.cond(
                (StageSignOffState.mht_analysis_tag == "none"),
                rx.text(""),
                rx.text(
                    f"{StageSignOffState.mht_analysis_tag}",
                    weight="bold",
                    size="1"
                ),
            ),
            style=content_box_style,
            direction="column",
            spacing="2"
        ),
        rx.flex(
            rx.text("Pixel Failure Analysis",
                    weight="bold"),
            style=content_box_style
        ),
        rx.flex(
            rx.text("Electrical Tests",
                    weight="bold"),
            style=content_box_style
        ),
        spacing="2",
        direction="row"
    )

def LLS_results_summary_pad() -> rx.Component:
    return rx.flex(
        rx.text("Summary of Test Results (E-Summaries)", 
                weight="bold"),
        position_row(),
        mht_row(),
        pfa_row(),
        ele_row(),
        style=content_box_style,
        spacing="2",
        direction="column"
    )

def position_row() -> rx.Component:
    return rx.hstack(
        rx.text("", width="5%"),
        rx.foreach(LLSState.mod_info, position_box),
        spacing="1"
    )


def mht_row() -> rx.Component:
    return rx.hstack(
        rx.text("MHT", weight="bold", width="5%"),
        rx.foreach(LLSState.mod_info, 
                   lambda modinfo, testinfo: mod_summary_box(modinfo, "MHT")),
        spacing="1"
    )


def pfa_row() -> rx.Component:
    return rx.hstack(
        rx.text("PFA", weight="bold", width="5%"),
        rx.foreach(LLSState.mod_info,
                   lambda modinfo, testinfo: mod_summary_box(modinfo, "PFA")),
        spacing="1"
    )


def ele_row() -> rx.Component:
    return rx.hstack(
        rx.text("ELE", weight="bold", width="5%"),
        rx.foreach(LLSState.mod_info, 
                   lambda modinfo, testinfo: mod_summary_box(modinfo, "ELE")),
        spacing="1"
    )


def position_box(modinfo: ModuleInfo) -> rx.Component:
    return rx.box(rx.text(modinfo.position, size="1", weight="bold"),
        width="22px",
        height="22px",
        text_align="center",
    )


def mod_summary_box(modinfo: ModuleInfo, test: str) -> rx.Component:
    return rx.box(" ",
        width="22px",
        height="22px",
        background_color=modinfo.serialnumber_color,
        border_width="1px",
        border_color="dimgray",
        on_click=StageSignOffState.copy_esummary(modinfo.position)   
    )


def report_flex() -> rx.Component:
    return rx.flex(
        e_summary_pad(),
        fe_reports(),
        direction="row",
        spacing="2"
    )


def e_summary_pad() -> rx.Component:
    return rx.flex(
        rx.text(f"E-Summary of the module at position no. {StageSignOffState.modpos_of_esummary}", weight="bold"),
        rx.scroll_area(
            rx.code_block(
                StageSignOffState.esummary_text,
                language="json",
                font_size="13px",
                border_radius="10px",
            ),
            type="always",
            scrollbars="vertical",
            style={"height": 450},
            padding_right="20px"
        ),
        style=content_box_style,
        direction="column",
        spacing="2"
    )


def fe_reports() -> rx.Component:
    return rx.flex(
        rx.text(f"Front-end test reports of the module at position no. {StageSignOffState.modpos_of_esummary}", weight="bold"),
        rx.scroll_area(
            rx.code_block(
                StageSignOffState.fe_mht_reports_text,
                language="json",
                font_size="13px",
                border_radius="10px",
            ),
#            rx.box("Hello World!!!"),
            type="always",
            scrollbars="vertical",
            style={"height": 450},
            padding_right="20px"
        ),
        style=content_box_style,
        direction="column",
        spacing="2"      
    )
    
