# itk-demo-qc-analysis

Analysis of quality-control data of loaded local supports

In the ITk Pixel project loaded local support (LLS) structures comprise a substantial number of detector modules. The analysis of data taken in quality-control (QC) measurements is an important production step and calls for an efficient management of the required analysis steps. The LLS QC analysis packages is based on three main components:

1) The analysis manager as a user interface and orchestration tool.
2) The local LLS QC data base (also known as configuration data base).
3) The QC container which provides the analysis programms.

The Analysis Manager
====================
The analysis manager is browser based and offers different functions to the user.

1) Select an LLS whose data should be analysed.
2) Submit analysis jobs for different modules of the LLS to the QC container
3) Display the results of the analyses
4) Prepare a stage sign off of the selected LLS

Start of the Analysis Manager
----------------------------- 
- Go to directory "analysis_manager_ui"
- Execute: reflex run

Selection of an LLS
------------------- 
The LLS to be dealt with is selected in the LLS panel in the upper left corner of the Analysis Manager. To load a list of available LLS from the LLS QC DB press the button "Get LLS Info". The pull down menu labelled "Select an LLS ID" is used to select one of the LLS. Once the LLS is selected its ID will also be shown below the pull down menu.
Use the button "Load LLS properties" to obtain the properties of the selected LLS from the DB. One of the properties is number of modules loaded on the local support. The type of the LLS and the number of modules will be displayed. Another important feature is the scan data available for an LLS.

Submission of analyses of scan data
===================================
The Analysis Composition panel is used to select the modules of the LLS for which a Minimum Health Test (MHT) is run. 
Each module of an LLS is represented by a box in the panel. 
The boxes are labelled with the position number of the module on the LLS and provide the module serial number. 
As a first step, the scan data to be analysed must be selected. The different data sets are flagged with a scan tag. 
The scan tag is selected from a pull down menu in the upper left corner of the Analysis Composition panel. 
Once a scan tag is selected the information on the scans must be loaded by pressing the button "Load tagged scan information". 
After loading the information three small boxes appear in each module box. 
These boxes are labelled "d", "a" and "t" and represent the scan data of a digital scan (d), analog scan (a), and a threshold scan (t). 
If data with the selected tag are available the box is shown in green colour, if not the box is red. 
Each module box contains a check box to select the module for analysis. 
Once one at least one module is selected a "Submit" button and a field to specfiy an analysis tags is displayed in the upper right corner of the Analysis Composition panel.
Use the "Submit" button to send analysis requests to the QC container for all selected modules. 
A separate request is sent for each module.

How to combine the analyses of different scans to one analysis tag
------------------------------------------------------------------
In some cases data with different scan tags for different modules shall be combine to the same analysis tag. 
This can be achieve my multiple submissions of analyses with the same analysis tag. 
This procedure is illustrated in the following example:
- Consider an LLS with 36 modules.
- Three sets of scans are meant to be used for stage sign off of the LLS. The tags of these scans are:
  scans_20240930_01, scans_20241001_03, and scans_20241005_01
- The scans with tag scans_20240930_01 are supposed to cover modules 1 to 7, 15, and 20 to 25.
  The scans with tag scans_20241001_03 cover modules 8 to 14, and 26 - 29.
  The scans with tag scans_20241005_01 cover modules 16 to 19, and 30 to 36.
- The analyses of these scan data can be attached to one analysis tag by submitting three sets of analyses with the same tag.
  First, select the appropriate scan tag. Second, select the modules for which data are supposed to be analysed, 
  and then define an analysis tag. Repeat this procedure two more times for the corresponding groups of modules.

Display of analysis results
===========================
Select the "Results Viewer" panel in the navigation bar. Select an analysis tag in the pull-down menu. 
Press the button "Load tagged results".
If there are no analysis results available at all for the selected LLS the "Results Viewer" panel will show the message "There are no analysis tags for LLS XXX." 
where "XXX" represents the ID of the selected LLS.
The "Results Viewer" panel contains a module box for each module. 
The boxes are labelled with the position number of the module on the LLS and provide the module serial number.
In each module box, there are four blocks which represent the four front-end chips of each ITk-Pixel quad module. 
In each front-end box, there are three boxes which represent the tests that constitute the MHT. 
If a front-end chip passed the test, the box is shown in green, if not, the box is shown in red. 
A legend explaining the meaning of the acronyms representing the tests can displayed by pushing the button "Show legend". 



